package pl.softwareskill.course.testing.examples.mockito.service;

import lombok.RequiredArgsConstructor;
import pl.softwareskill.course.testing.examples.mockito.domain.Basket;
import pl.softwareskill.course.testing.examples.mockito.domain.BasketRepository;
import pl.softwareskill.course.testing.examples.mockito.domain.OrderService;

import java.util.UUID;

@RequiredArgsConstructor
public class CreateOrderService {

    private final BasketRepository basketRepository;
    private final OrderService orderService;

    public String createOrder(UUID basketId) {
        Basket basket = basketRepository.getById(basketId)
                .orElseThrow(this::onBasketNotFound);

        if (basket.getInsertedProducts().isEmpty()) {
            throw new IllegalStateException("Cannot create order from empty basket.");
        }

        return orderService.createOrder(basket);
    }

    private RuntimeException onBasketNotFound() {
        return new IllegalArgumentException();
    }
}
