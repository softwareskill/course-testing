package pl.softwareskill.course.testing.examples.mockito.domain;

public interface OrderService {

    String createOrder(Basket basket);
}
