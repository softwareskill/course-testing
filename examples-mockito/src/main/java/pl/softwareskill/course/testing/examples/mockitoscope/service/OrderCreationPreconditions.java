package pl.softwareskill.course.testing.examples.mockitoscope.service;

import pl.softwareskill.course.testing.examples.mockitoscope.domain.Product;
import pl.softwareskill.course.testing.examples.mockitoscope.domain.Basket;

class OrderCreationPreconditions {

    boolean emptyBasket(Basket basket) {
        return basket.getInsertedProducts().isEmpty();
    }

    boolean allProductsAreAvailable(Basket basket) {
        return basket.getInsertedProducts()
                .stream()
                .allMatch(Product::isAvailable);
    }
}
