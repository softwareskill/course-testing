package pl.softwareskill.course.testing.examples.mockitoscope.domain;

import java.math.BigDecimal;

public interface DiscountPolicy {

    BigDecimal getDiscount(Basket basket);
}
