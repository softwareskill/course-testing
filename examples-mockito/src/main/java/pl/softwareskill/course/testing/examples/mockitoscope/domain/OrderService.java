package pl.softwareskill.course.testing.examples.mockitoscope.domain;

public interface OrderService {

    String createOrder(Basket basket);
}
