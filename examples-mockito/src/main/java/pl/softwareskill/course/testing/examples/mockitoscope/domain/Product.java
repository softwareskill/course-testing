package pl.softwareskill.course.testing.examples.mockitoscope.domain;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.util.UUID;

@Value
@Builder
public class Product {

    UUID productId;
    String name;
    BigDecimal price;
    boolean available;
}
