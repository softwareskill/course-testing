package pl.softwareskill.course.testing.examples.mockito.domain;

import java.math.BigDecimal;

public interface DiscountPolicy {

    BigDecimal getDiscount(Basket basket);
}
