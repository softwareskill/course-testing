package pl.softwareskill.course.testing.examples.mockito.fakers;

import pl.softwareskill.course.testing.examples.mockito.domain.Basket;
import pl.softwareskill.course.testing.examples.mockito.domain.DiscountPolicy;

import java.math.BigDecimal;

public class DiscountPolicyStub implements DiscountPolicy {

    @Override
    public BigDecimal getDiscount(Basket basket) {
        return BigDecimal.valueOf(10);
    }
}
