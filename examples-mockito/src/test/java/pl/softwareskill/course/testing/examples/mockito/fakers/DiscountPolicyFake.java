package pl.softwareskill.course.testing.examples.mockito.fakers;

import lombok.RequiredArgsConstructor;
import pl.softwareskill.course.testing.examples.mockito.domain.Basket;
import pl.softwareskill.course.testing.examples.mockito.domain.DiscountPolicy;

import java.math.BigDecimal;

@RequiredArgsConstructor
public class DiscountPolicyFake implements DiscountPolicy {

    private final BigDecimal discount;

    @Override
    public BigDecimal getDiscount(Basket basket) {
        return discount;
    }
}
