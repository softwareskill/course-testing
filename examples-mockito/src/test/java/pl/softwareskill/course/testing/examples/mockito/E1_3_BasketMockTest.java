package pl.softwareskill.course.testing.examples.mockito;

import org.junit.jupiter.api.Test;
import pl.softwareskill.course.testing.examples.mockito.domain.Basket;
import pl.softwareskill.course.testing.examples.mockito.domain.DiscountPolicy;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class E1_3_BasketMockTest {

    @Test
    void calculatesFinalPrice() {
        var basket = givenBasketWithProducts();
        var discount = BigDecimal.valueOf(10);
        var discountPolicy = mock(DiscountPolicy.class);
        when(discountPolicy.getDiscount(basket)).thenReturn(discount);

        var finalPrice = basket.getFinalPrice(discountPolicy);

        assertThat(finalPrice)
                .isEqualTo(basket.getItemsTotalPrice().subtract(discount));
    }

    // BDD example
    @Test
    void calculatesFinalPrice2() {
        var basket = givenBasketWithProducts();
        var discount = BigDecimal.valueOf(10);
        var discountPolicy = mock(DiscountPolicy.class);
        given(discountPolicy.getDiscount(basket)).willReturn(discount);

        var finalPrice = basket.getFinalPrice(discountPolicy);

        assertThat(finalPrice)
                .isEqualTo(basket.getItemsTotalPrice().subtract(discount));
    }

    private static Basket givenBasketWithProducts() {
        var basket = new Basket();
        basket.insert(ProductFactory.sample());
        basket.insert(ProductFactory.sample());
        return basket;
    }

}