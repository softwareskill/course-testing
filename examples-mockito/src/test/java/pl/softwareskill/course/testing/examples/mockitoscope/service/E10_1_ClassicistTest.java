package pl.softwareskill.course.testing.examples.mockitoscope.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.softwareskill.course.testing.examples.mockitoscope.ProductFactory;
import pl.softwareskill.course.testing.examples.mockitoscope.domain.Basket;
import pl.softwareskill.course.testing.examples.mockitoscope.domain.BasketRepository;
import pl.softwareskill.course.testing.examples.mockitoscope.domain.OrderService;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;

@ExtendWith(MockitoExtension.class)
class E10_1_ClassicistTest {

    @Mock
    BasketRepository basketRepository;
    @Mock
    OrderService orderService;

    CreateOrderService createOrderService;

    @BeforeEach
    void setup() {
        createOrderService = new CreateOrderService(basketRepository, orderService, new OrderCreationPreconditions());
    }

    @Test
    @DisplayName("Creates order for basket")
    void createsOrderForBasket() {
        // given
        Basket basket = givenBasketWithProducts();
        given(basketRepository.getById(basket.getBasketId())).willReturn(Optional.of(basket));

        // when
        createOrderService.createOrder(basket.getBasketId());

        // then
        then(orderService)
                .should()
                .createOrder(basket);
    }

    @Test
    @DisplayName("Does not create order for empty basket")
    void doesNotCreateOrderForEmptyBasket() {
        // given
        Basket basket = givenBasketWithoutProducts();
        given(basketRepository.getById(basket.getBasketId())).willReturn(Optional.of(basket));

        // when
        assertThatCode(() -> createOrderService.createOrder(basket.getBasketId()))
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("Cannot create order from empty basket.");

        // then
        then(orderService)
                .should(never())
                .createOrder(basket);
    }

    @Test
    @DisplayName("Does not create order for basket with not active product")
    void doesNotCreateOrderForBasketWithNotActiveProduct() {
        // given
        Basket basket = givenBasketWithoutProducts();
        basket.insert(ProductFactory.sample());
        basket.insert(ProductFactory.notActiveProduct());
        given(basketRepository.getById(basket.getBasketId())).willReturn(Optional.of(basket));

        // when
        assertThatCode(() -> createOrderService.createOrder(basket.getBasketId()))
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("Some product is not available.");

        // then
        then(orderService)
                .should(never())
                .createOrder(basket);
    }

    private static Basket givenBasketWithProducts() {
        var basket = new Basket();
        basket.insert(ProductFactory.sample());
        basket.insert(ProductFactory.sample());
        return basket;
    }

    private static Basket givenBasketWithoutProducts() {
        var basket = new Basket();
        return basket;
    }
}