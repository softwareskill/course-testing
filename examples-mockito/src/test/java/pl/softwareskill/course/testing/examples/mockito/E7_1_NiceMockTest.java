package pl.softwareskill.course.testing.examples.mockito;

import lombok.Data;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
class E7_1_NiceMockTest {

    @Data
    class Session {
        String sessionName;
        Connection connection;
    }

    @Data
    class Connection {
        boolean active;
        int startTimestamp;
        Address address;
    }

    @Data
    class Address {
        String ip;
        int port;
    }

    @Test
    void standardMock() {
        var mock = mock(Session.class);

        assertThat(mock.getSessionName()).isNull();
        assertThat(mock.getConnection()).isNull();
    }

    @Test
    void smartNulls() {
        var mock = mock(Session.class, Answers.RETURNS_SMART_NULLS);

        assertThat(mock.getSessionName()).isEqualTo("");
        // nice mock
        assertThat(mock.getConnection()).isNotNull();
        // not null, but cannot call methods
    }

    @Test
    void returnsMocks() {
        var mock = mock(Session.class, Answers.RETURNS_MOCKS);

        assertThat(mock.getConnection()).isNotNull();
        // not null
        assertThat(mock.getConnection().getAddress()).isNotNull();
        // not null
        assertThat(mock.getConnection().getAddress().getIp()).isNotNull(); // like smart null
        // not null
    }

    @Test
    void returnsDeepStubs() {
        var mock = mock(Session.class, Answers.RETURNS_DEEP_STUBS);

        assertThat(mock.getConnection()).isNotNull();
        // not null
        assertThat(mock.getConnection().getAddress()).isNotNull();
        // not null
        assertThat(mock.getConnection().getAddress().getIp()).isNull();
        // not null
    }
}
