package pl.softwareskill.course.testing.examples.mockito;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.softwareskill.course.testing.examples.mockito.domain.Basket;
import pl.softwareskill.course.testing.examples.mockito.domain.DiscountPolicy;
import pl.softwareskill.course.testing.examples.mockito.domain.Product;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class E1_5_AnypatternBasketMockTest {

    @Mock
    DiscountPolicy discountPolicy;

    // THIS IS ANTY-PATTERN EXAMPLE
    @Test
    void calculatesFinalPriceAntyPattern() {
        var basket = givenBasketWithProducts();
        var discount = BigDecimal.valueOf(10);
        given(discountPolicy.getDiscount(basket)).willReturn(discount);

        var finalPrice = basket.getFinalPrice(discountPolicy);

        assertThat(finalPrice)
                .isEqualTo(basket.getItemsTotalPrice().subtract(discount));
    }

    private Basket givenBasketWithProducts() {
        // antipattern
        var product1 = mock(Product.class);
        var product2 = mock(Product.class);
        when(product1.getPrice()).thenReturn(BigDecimal.valueOf(10));
        when(product2.getPrice()).thenReturn(BigDecimal.valueOf(20));
        // end of antipattern

        var basket = new Basket();
        basket.insert(product1);
        basket.insert(product2);
        return basket;
    }
}