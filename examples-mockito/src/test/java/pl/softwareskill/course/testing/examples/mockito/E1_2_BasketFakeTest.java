package pl.softwareskill.course.testing.examples.mockito;

import org.junit.jupiter.api.Test;
import pl.softwareskill.course.testing.examples.mockito.domain.Basket;
import pl.softwareskill.course.testing.examples.mockito.fakers.DiscountPolicyFake;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class E1_2_BasketFakeTest {

    @Test
    void calculatesFinalPrice() {
        var discount = BigDecimal.valueOf(10);
        var basket = givenBasketWithProducts();
        var discountPolicy = new DiscountPolicyFake(discount);

        var finalPrice = basket.getFinalPrice(discountPolicy);

        assertThat(finalPrice)
                .isEqualTo(basket.getItemsTotalPrice().subtract(discount));
    }

    private static Basket givenBasketWithProducts() {
        var basket = new Basket();
        basket.insert(ProductFactory.sample());
        basket.insert(ProductFactory.sample());
        return basket;
    }

}