package pl.softwareskill.course.testing.examples.mockito;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.softwareskill.course.testing.examples.mockito.domain.Basket;
import pl.softwareskill.course.testing.examples.mockito.domain.DiscountPolicy;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class E1_4_BasketMockExtensionTest {

    @Mock
    DiscountPolicy discountPolicy;

    @Test
    void calculatesFinalPrice2() {
        var basket = givenBasketWithProducts();
        var discount = BigDecimal.valueOf(10);
        given(discountPolicy.getDiscount(basket)).willReturn(discount);

        var finalPrice = basket.getFinalPrice(discountPolicy);

        assertThat(finalPrice)
                .isEqualTo(basket.getItemsTotalPrice().subtract(discount));
    }

    private static Basket givenBasketWithProducts() {
        var basket = new Basket();
        basket.insert(ProductFactory.sample());
        basket.insert(ProductFactory.sample());
        return basket;
    }

}