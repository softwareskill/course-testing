package pl.softwareskill.course.testing.examples.mockito;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.softwareskill.course.testing.examples.mockito.domain.Basket;
import pl.softwareskill.course.testing.examples.mockito.domain.BasketRepository;
import pl.softwareskill.course.testing.examples.mockito.domain.OrderService;
import pl.softwareskill.course.testing.examples.mockito.service.CreateOrderService;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;

@ExtendWith(MockitoExtension.class)
class E6_1_ArgumentCaptorTest {

    @Mock
    BasketRepository basketRepository;
    @Mock
    OrderService orderService;

    @InjectMocks
    CreateOrderService createOrderService;

    @Captor
    ArgumentCaptor<Basket> basketArgumentCaptor;

    @Test
    void usesBasketToCreateAnOrder() {
        // given
        Basket basket = givenBasketWithProducts();
        given(basketRepository.getById(basket.getBasketId())).willReturn(Optional.of(basket));

        given(orderService.createOrder(basketArgumentCaptor.capture()))
                .willReturn(null);

        // when
        createOrderService.createOrder(basket.getBasketId());

        // then
        assertThat(basketArgumentCaptor.getValue())
                .isEqualTo(basket);
    }

    private static Basket givenBasketWithProducts() {
        var basket = new Basket();
        basket.insert(ProductFactory.sample());
        basket.insert(ProductFactory.sample());
        return basket;
    }
}
