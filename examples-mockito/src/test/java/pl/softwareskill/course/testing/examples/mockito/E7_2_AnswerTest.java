package pl.softwareskill.course.testing.examples.mockito;

import lombok.Data;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import pl.softwareskill.course.testing.examples.mockito.domain.Basket;
import pl.softwareskill.course.testing.examples.mockito.domain.BasketRepository;
import pl.softwareskill.course.testing.examples.mockito.domain.OrderService;
import pl.softwareskill.course.testing.examples.mockito.service.CreateOrderService;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
class E7_2_AnswerTest {

    @Mock
    BasketRepository basketRepository;
    @Mock
    OrderService orderService;

    @InjectMocks
    CreateOrderService createOrderService;

    @Test
    void returnsNewOrderId() {
        // given
        Basket basket = givenBasketWithProducts();
        given(basketRepository.getById(basket.getBasketId())).willReturn(Optional.of(basket));

        given(orderService.createOrder(any(Basket.class))).willAnswer(new Answer<>() {
            @Override
            public String answer(InvocationOnMock invocation) {
                return invocation.getArgument(0, Basket.class)
                        .getBasketId()
                        .toString() + "-basket-order";
            }
        });

        // when
        String orderId = createOrderService.createOrder(basket.getBasketId());

        // then
        assertThat(orderId)
                .isEqualTo(basket.getBasketId() + "-basket-order");
    }

    private static Basket givenBasketWithProducts() {
        var basket = new Basket();
        basket.insert(ProductFactory.sample());
        basket.insert(ProductFactory.sample());
        return basket;
    }
}
