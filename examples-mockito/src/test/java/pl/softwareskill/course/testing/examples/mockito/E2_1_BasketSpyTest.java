package pl.softwareskill.course.testing.examples.mockito;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.softwareskill.course.testing.examples.mockito.domain.Basket;
import pl.softwareskill.course.testing.examples.mockito.domain.DiscountPolicy;
import pl.softwareskill.course.testing.examples.mockito.fakers.DiscountPolicyStub;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class E2_1_BasketSpyTest {

    DiscountPolicy discountPolicy = new DiscountPolicyStub();

    @Test
    void calculatesFinalPrice() {
        var basket = givenBasketWithProducts();

        // can be done by @Spy as well
        var spyDiscountPolicy = spy(discountPolicy);

        var finalPrice = basket.getFinalPrice(spyDiscountPolicy);

        // then
        //verify(discountPolicy).getDiscount(basket);
        verify(spyDiscountPolicy).getDiscount(basket);

        assertThat(finalPrice)
                .isEqualTo(basket.getItemsTotalPrice().subtract(BigDecimal.valueOf(10)));
    }

    @Test
    void calculatesFinalPrice2() {
        var basket = givenBasketWithProducts();

        var spyDiscountPolicy = spy(discountPolicy);
        // change behaviour
        when(spyDiscountPolicy.getDiscount(basket))
                .thenReturn(BigDecimal.valueOf(20));

        var finalPrice = basket.getFinalPrice(spyDiscountPolicy);

        // then

        // still can verify
        verify(spyDiscountPolicy).getDiscount(basket);

        assertThat(finalPrice)
                .isEqualTo(basket.getItemsTotalPrice().subtract(BigDecimal.valueOf(20)));
    }

    private static Basket givenBasketWithProducts() {
        var basket = new Basket();
        basket.insert(ProductFactory.sample());
        basket.insert(ProductFactory.sample());
        return basket;
    }
}
