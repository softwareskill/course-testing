package pl.softwareskill.course.testing.examples.mockito;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.softwareskill.course.testing.examples.mockito.domain.Basket;
import pl.softwareskill.course.testing.examples.mockito.domain.BasketRepository;
import pl.softwareskill.course.testing.examples.mockito.domain.OrderService;
import pl.softwareskill.course.testing.examples.mockito.service.CreateOrderService;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class E4_1_VerifyMocksTest {

    @Mock
    BasketRepository basketRepository;
    @Mock
    OrderService orderService;

    @InjectMocks
    CreateOrderService createOrderService;

    @Test
    void createsOrderForBasket() {
        // given
        Basket basket = givenBasketWithProducts();
        given(basketRepository.getById(basket.getBasketId())).willReturn(Optional.of(basket));

        // when
        createOrderService.createOrder(basket.getBasketId());

        // then
        verify(orderService).createOrder(basket);
    }

    @Test
    void doesNotCreateOrderForEmptyBasket() {
        // given
        Basket basket = givenBasketWithoutProducts();
        given(basketRepository.getById(basket.getBasketId())).willReturn(Optional.of(basket));

        // when
        assertThatCode(() -> createOrderService.createOrder(basket.getBasketId()))
                .isInstanceOf(IllegalStateException.class);

        // then
        verify(orderService, never()).createOrder(basket);
    }

    private static Basket givenBasketWithProducts() {
        var basket = new Basket();
        basket.insert(ProductFactory.sample());
        basket.insert(ProductFactory.sample());
        return basket;
    }

    private static Basket givenBasketWithoutProducts() {
        var basket = new Basket();
        return basket;
    }
}
