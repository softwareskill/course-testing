package pl.softwareskill.course.testing.examples.mockito;

import org.junit.jupiter.api.Test;
import pl.softwareskill.course.testing.examples.mockito.domain.Basket;
import pl.softwareskill.course.testing.examples.mockito.fakers.DiscountPolicyStub;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class E1_1_BasketStubTest {

    @Test
    void calculatesFinalPrice() {
        var basket = givenBasketWithProducts();
        var discountPolicy = new DiscountPolicyStub();

        var finalPrice = basket.getFinalPrice(discountPolicy);

        assertThat(finalPrice)
                .isEqualTo(basket.getItemsTotalPrice().subtract(BigDecimal.valueOf(10)));
    }

    private static Basket givenBasketWithProducts() {
        var basket = new Basket();
        basket.insert(ProductFactory.sample());
        basket.insert(ProductFactory.sample());
        return basket;
    }

}