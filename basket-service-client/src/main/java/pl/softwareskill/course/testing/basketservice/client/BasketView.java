package pl.softwareskill.course.testing.basketservice.client;

import lombok.Data;

import java.util.UUID;

@Data
public class BasketView {

    private UUID basketId;
}
