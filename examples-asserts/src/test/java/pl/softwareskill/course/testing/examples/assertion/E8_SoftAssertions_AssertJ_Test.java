package pl.softwareskill.course.testing.examples.assertion;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@Disabled
class E8_SoftAssertions_AssertJ_Test {

    @Test
    void standardAssertions() {
        var product = ProductFactory.sample();

        assertThat(product.getName())
                .containsIgnoringCase("NOT IN NAME");
        assertThat(product.getPrice())
                .isEqualTo(BigDecimal.ZERO);
    }

    @Test
    void softAssertions() {
        var product = ProductFactory.sample();

        SoftAssertions softly = new SoftAssertions();

        softly.assertThat(product.getName())
                .as("name")
                .containsIgnoringCase("NOT IN NAME");
        softly.assertThat(product.getPrice())
                .as("price")
                .isEqualTo(BigDecimal.ZERO);

        softly.assertAll();
    }
}
