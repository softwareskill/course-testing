package pl.softwareskill.course.testing.examples.assertion.equals;

import com.google.common.testing.EqualsTester;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class E1_EqualsTester_Test {

    /**
     * This tests:
     *
     * comparing each object against itself returns true
     * comparing each object against null returns false
     * comparing each object against an instance of an incompatible class returns false
     * comparing each pair of objects within the same equality group returns true
     * comparing each pair of objects from different equality groups returns false
     * the hash codes of any two equal objects are equal
     *
     * When a test fails, the error message labels the objects involved in the failed comparison as follows:
     *
     * "[group i, item j]" refers to the jth item in the ith equality group, where both equality groups and the items
     * within equality groups are numbered starting from 1. When either a constructor argument or an equal object
     * is provided, that becomes group 1.
     */
    @Test
    @Disabled("Enable it explicitly")
    void checkEquals() {
        new EqualsTester()
                .addEqualityGroup(new Person("Josh", 30), new Person("Josh", 30))
                .addEqualityGroup(new Person("Josh", 25), new Person("Josh", 25))
                .addEqualityGroup(new Person("Ana", 25), new Person("Ana", 25))
                .testEquals();
    }
}
