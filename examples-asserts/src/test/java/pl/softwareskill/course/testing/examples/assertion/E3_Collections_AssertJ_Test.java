package pl.softwareskill.course.testing.examples.assertion;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class E3_Collections_AssertJ_Test {

    @Test
    void emptyBasket() {
        var basket = new Basket();

        assertThat(basket.getInsertedProducts())
                .isEmpty();
    }

    @Test
    void singleProduct() {
        var basket = new Basket();
        var product = ProductFactory.sample();

        basket.insert(product);

        assertThat(basket.getInsertedProducts())
                .hasSize(1)
                .containsOnly(product);
    }

    @Test
    void multipleProducts() {
        var basket = new Basket();
        var productA = ProductFactory.sample();
        var productB = ProductFactory.sample();

        basket.insert(productA);
        basket.insert(productB);
        basket.insert(productA);

        assertThat(basket.getInsertedProducts())
                .hasSize(3)
                .containsExactlyInAnyOrder(productA, productA, productB);
    }
}
