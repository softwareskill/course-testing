package pl.softwareskill.course.testing.examples.assertion;

import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class E5_Optional_AssertJ_Test {

    @Test
    void nonEmpty() {
        var product = ProductFactory.sample();

        // fake filtering
        Optional<Product> found = Stream.of(product)
                .filter(p -> p.getName().contains("Sample"))
                .findFirst();

        assertThat(found)
                .isNotEmpty()
                .contains(product);
    }

    @Test
    void empty() {
        var product = ProductFactory.sample();

        // fake filtering
        Optional<Product> found = Stream.of(product)
                .filter(p -> p.getName().contains("Fake"))
                .findFirst();

        assertThat(found)
                .isEmpty();
    }
}
