package pl.softwareskill.course.testing.examples.assertion.exceptions;

import org.junit.jupiter.api.Test;
import pl.softwareskill.course.testing.examples.assertion.Basket;

import static org.assertj.core.api.Assertions.*;

class E1_Exception_AssertJ_Test {

    @Test
    // prior Junit4: @Test(expected = NullPointerException.class)
    void invalidExceptionScope() {
        try {
            var basket = givenEmptyBasket();
            basket.insert(null);
        } catch (Exception exception) {
            assertThat(exception)
                .isInstanceOf(NullPointerException.class);

            return;
        }

        fail("exception should be raised");
    }

    @Test
    void narrowExceptionScope() {
        var basket = givenEmptyBasket();

        try {
            basket.insert(null);
        } catch (Exception exception) {
            assertThat(exception)
                    .isInstanceOf(NullPointerException.class);

            return;
        }

        fail("exception should be raised");
    }

    @Test
    void lambdaExceptionScope() {
        var basket = givenEmptyBasket();

        assertThatCode(() -> basket.insert(null))
                .isInstanceOf(NullPointerException.class);
    }

    private Basket givenEmptyBasket() {
//        throw new NullPointerException("error while initialization!");
        return new Basket();
    }
}
