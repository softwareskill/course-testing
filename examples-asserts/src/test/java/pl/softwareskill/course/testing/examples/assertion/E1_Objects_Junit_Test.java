package pl.softwareskill.course.testing.examples.assertion;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


class E1_Objects_Junit_Test {

    @Test
    void checkName() {
        var product = ProductFactory.sample();

        assertNotNull(product.getProductId());
    }

    @Test
    void instance() {
        var product = ProductFactory.sample();

        assertTrue(product.getProductId() instanceof UUID);
    }

    @Test
    void booleans() {
        var product = ProductFactory.sample();

        assertTrue(product.isAvailable());
    }
}
