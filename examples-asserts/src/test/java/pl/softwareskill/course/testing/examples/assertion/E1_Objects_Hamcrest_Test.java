package pl.softwareskill.course.testing.examples.assertion;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


class E1_Objects_Hamcrest_Test {

    @Test
    void nullable() {
        var product = ProductFactory.sample();

        assertThat(product.getProductId(), notNullValue());
    }

    @Test
    void instance() {
        var product = ProductFactory.sample();

        assertThat(product.getProductId(), instanceOf(UUID.class));
    }

    @Test
    void booleans() {
        var product = ProductFactory.sample();

        assertThat(product.isAvailable(), is(true));
    }
}
