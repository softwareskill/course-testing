package pl.softwareskill.course.testing.examples.assertion;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


class E4_Comparison_Hamcrest_Test {

    @Test
    void nonZeroPrice() {
        var product = ProductFactory.sample();

        assertThat(product.getPrice(), greaterThan(BigDecimal.ZERO));
    }

    @Test
    void between() {
        var product = ProductFactory.sample();

        assertThat(product.getPrice(), allOf(greaterThan(BigDecimal.valueOf(10)), lessThan(BigDecimal.valueOf(20))));
    }

    @Test
    void positivePrice() {
        var product = ProductFactory.sample();

        assertThat(product.getPrice(), greaterThanOrEqualTo(BigDecimal.ZERO));
    }
}
