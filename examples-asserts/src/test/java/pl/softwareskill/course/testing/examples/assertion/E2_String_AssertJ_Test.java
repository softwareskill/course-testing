package pl.softwareskill.course.testing.examples.assertion;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class E2_String_AssertJ_Test {

    @Test
    void checkName() {
        var product = ProductFactory.sample();

        assertThat(product.getName())
                .isEqualTo("Sample product");
    }

    @Test
    void checkContainsString() {
        var product = ProductFactory.sample();

        assertThat(product.getName())
                .contains("Sample");
    }

    @Test
    void checkBlankString() {
        var product = ProductFactory.sample();

        assertThat(product.getName())
                .isNotBlank();
    }

    @Test
    void checkChained() {
        var product = ProductFactory.sample();

        assertThat(product.getName())
                .isNotBlank()
                .containsIgnoringCase("sample product");
    }
}
