package pl.softwareskill.course.testing.examples.assertion;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class E2_String_Junit_Test {

    @Test
    void checkName() {
        var product = ProductFactory.sample();

        assertEquals("Sample product", product.getName());
    }

    @Test
    void checkContainsString() {
        var product = ProductFactory.sample();

        assertTrue(product.getName().contains("Sample product"));
    }

    @Test
    void checkBlankString() {
        var product = ProductFactory.sample();

        assertFalse(product.getName().isBlank());
    }

    @Test
    void checkChained() {
        var product = ProductFactory.sample();

        assertFalse(product.getName().isBlank());
        assertTrue(product.getName().toLowerCase().contains("sample product"));
    }
}
