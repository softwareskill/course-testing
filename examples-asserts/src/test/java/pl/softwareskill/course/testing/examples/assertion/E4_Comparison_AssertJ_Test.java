package pl.softwareskill.course.testing.examples.assertion;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class E4_Comparison_AssertJ_Test {

    @Test
    void nonZeroPrice() {
        var product = ProductFactory.sample();

        assertThat(product.getPrice())
                .isGreaterThan(BigDecimal.ZERO);
    }

    @Test
    void between() {
        var product = ProductFactory.sample();

        assertThat(product.getPrice())
                .isBetween(BigDecimal.valueOf(10), BigDecimal.valueOf(20));
    }

    @Test
    void positivePrice() {
        var product = ProductFactory.sample();

        assertThat(product.getPrice())
                .isPositive();
    }
}
