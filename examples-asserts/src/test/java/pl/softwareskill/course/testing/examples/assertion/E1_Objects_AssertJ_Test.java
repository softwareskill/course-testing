package pl.softwareskill.course.testing.examples.assertion;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class E1_Objects_AssertJ_Test {

    @Test
    void nullable() {
        var product = ProductFactory.sample();

        assertThat(product.getProductId())
                .isNotNull();
    }

    @Test
    void instance() {
        var product = ProductFactory.sample();

        assertThat(product.getProductId())
                .isInstanceOf(UUID.class);
    }

    @Test
    void booleans() {
        var product = ProductFactory.sample();

        assertThat(product.isAvailable())
                .isTrue();
    }
}
