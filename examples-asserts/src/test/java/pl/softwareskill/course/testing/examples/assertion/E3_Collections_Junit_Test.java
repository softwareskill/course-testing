package pl.softwareskill.course.testing.examples.assertion;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;


class E3_Collections_Junit_Test {

    @Test
    void checkName() {
        var basket = new Basket();

        assertTrue(basket.getInsertedProducts().isEmpty());
    }
}
