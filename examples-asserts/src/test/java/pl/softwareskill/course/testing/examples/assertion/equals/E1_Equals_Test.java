package pl.softwareskill.course.testing.examples.assertion.equals;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class E1_Equals_Test {

    @Test
    void sameObject() {
        var person = new Person("Josh", 30);
        var number = Long.valueOf(30);

        assertThat(person).isNotEqualTo(number);
    }

    @Test
    void comparisonToNull() {
        var person = new Person("Josh", 30);

        assertThat(person).isNotEqualTo(null);
    }

    @Test
    void reflexive() {
        var person = new Person("Josh", 30);

        assertThat(person).isEqualTo(person);
    }

    @Test
    void symmetric() {
        var personA = new Person("Josh", 30);
        var personB = new Person("Josh", 25);

        assertThat(personA.equals(personB))
                .isEqualTo(personB.equals(personA));
    }

    @Test
    void transitive() {
        var personA = new Person("Josh", 30);
        var personB = new Person("Josh", 30);
        var personC = new Person("Josh", 30);

        assertThat(personA).isEqualTo(personB);
        assertThat(personB).isEqualTo(personC);
        assertThat(personA).isEqualTo(personC);
    }

    @Test
    void consistent() {
        var personA = new Person("Josh", 30);
        var personB = new Person("Josh", 30);

        personB.setAnnotation("Anotation");

        assertThat(personA)
                .isEqualTo(personB);
    }

    @Nested
    class Fields {

        @Test
        void name() {
            var personA = new Person("Josh", 30);
            var personB = new Person("Ana", 30);

            assertThat(personA)
                    .isNotEqualTo(personB);
        }

        @Test
        @Disabled("Intentionally skipped")
        void age() {
            var personA = new Person("Josh", 30);
            var personB = new Person("Josh", 25);

            assertThat(personA)
                    .isNotEqualTo(personB);
        }
    }
}
