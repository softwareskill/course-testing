package pl.softwareskill.course.testing.examples.assertion;

import java.math.BigDecimal;

import static java.util.UUID.randomUUID;

public class ProductFactory {

    public static Product sample() {
        return Product.builder()
                .productId(randomUUID())
                .name("Sample product")
                .price(new BigDecimal("19.99"))
                .available(true)
                .build();
    }
}
