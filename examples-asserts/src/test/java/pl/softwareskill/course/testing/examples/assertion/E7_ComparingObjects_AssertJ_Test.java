package pl.softwareskill.course.testing.examples.assertion;

import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;
import org.junit.jupiter.api.Test;
import pl.softwareskill.course.testing.examples.assertion.dto.Address;
import pl.softwareskill.course.testing.examples.assertion.dto.BasketSummary;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class E7_ComparingObjects_AssertJ_Test {

    @Test
    void comparingObjects() {
        var basketSummary = givenBasketSummary();

        var shippingAddress = new Address();
        shippingAddress.setCity("Warszawa");
        shippingAddress.setStreet("Aleje Jerozolimskie");
        shippingAddress.setNumber("1/1");
        var expected = new BasketSummary();
        expected.setShippingAddress(shippingAddress);

        assertThat(basketSummary)
                .usingRecursiveComparison(
                        RecursiveComparisonConfiguration.builder()
                                .withIgnoredFields("shippingAddress.country")
                                .build())
                .isEqualTo(expected);
    }

    private BasketSummary givenBasketSummary() {
        var shippingAddress = new Address();
        shippingAddress.setCity("Warszawa");
        shippingAddress.setStreet("Aleje Jerozolimskie");
        shippingAddress.setNumber("1/1");
        shippingAddress.setCountry("Polska");

        var basketSummary = new BasketSummary();
        basketSummary.setShippingAddress(shippingAddress);

        return basketSummary;
    }
}
