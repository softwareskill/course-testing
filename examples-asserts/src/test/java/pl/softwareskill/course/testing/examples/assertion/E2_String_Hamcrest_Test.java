package pl.softwareskill.course.testing.examples.assertion;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


class E2_String_Hamcrest_Test {

    @Test
    void checkName() {
        var product = ProductFactory.sample();

        assertThat(product.getName(), equalTo("Sample product"));
    }

    @Test
    void checkContainsString() {
        var product = ProductFactory.sample();

        assertThat(product.getName(), containsString("Sample product"));
    }

    @Test
    void checkBlankString() {
        var product = ProductFactory.sample();

        assertThat(product.getName(), not(blankString()));
    }

    @Test
    void checkChained() {
        var product = ProductFactory.sample();

        assertThat(product.getName(), allOf(
                not(blankString()),
                containsStringIgnoringCase("sample product")));
    }
}
