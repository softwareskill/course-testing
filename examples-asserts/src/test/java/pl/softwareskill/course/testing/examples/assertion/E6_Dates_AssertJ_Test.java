package pl.softwareskill.course.testing.examples.assertion;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class E6_Dates_AssertJ_Test {

    @Test
    void after() {
        var yesterday = LocalDate.now().minusDays(1);

        assertThat(LocalDate.now())
                .isAfter(yesterday);
    }
}
