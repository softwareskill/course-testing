package pl.softwareskill.course.testing.examples.assertion;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


class E3_Collections_Hamcrest_Test {

    @Test
    void emptyBasket() {
        var basket = new Basket();

        assertThat(basket.getInsertedProducts(), empty());
    }

    @Test
    void singleProduct() {
        var basket = new Basket();
        var product = ProductFactory.sample();

        basket.insert(product);

//        assertThat(basket.getInsertedProducts(), allOf(iterableWithSize(1), contains(product)));
        assertThat(basket.getInsertedProducts(), hasSize(1));
        assertThat(basket.getInsertedProducts(), contains(product));
    }

    @Test
    void multipleProducts() {
        var basket = new Basket();
        var productA = ProductFactory.sample();
        var productB = ProductFactory.sample();

        basket.insert(productA);
        basket.insert(productB);
        basket.insert(productA);

        assertThat(basket.getInsertedProducts(), containsInAnyOrder(productA, productA, productB));
    }
}
