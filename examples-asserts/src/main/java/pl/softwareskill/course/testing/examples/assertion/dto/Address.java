package pl.softwareskill.course.testing.examples.assertion.dto;

import lombok.Data;

@Data
public class Address {

    private String country;
    private String city;
    private String street;
    private String number;
}
