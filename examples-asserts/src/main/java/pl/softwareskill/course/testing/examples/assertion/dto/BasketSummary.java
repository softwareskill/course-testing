package pl.softwareskill.course.testing.examples.assertion.dto;

import lombok.Data;

@Data
public class BasketSummary {

    private Address shippingAddress;
}
