package pl.softwareskill.course.testing.examples.assertion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import static java.util.Objects.requireNonNull;

public class Basket {

    private final UUID basketId;
    private final Collection<Product> products;

    public Basket() {
        this.basketId = UUID.randomUUID();
        this.products = new ArrayList<>();
    }

    public UUID getBasketId() {
        return basketId;
    }

    public void insert(Product product) {
        requireNonNull(product);
        products.add(product);
    }

    public Collection<Product> getInsertedProducts() {
        return new ArrayList<>(products);
    }
}
