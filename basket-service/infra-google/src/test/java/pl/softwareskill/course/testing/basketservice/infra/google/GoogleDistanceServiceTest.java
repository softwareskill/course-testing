package pl.softwareskill.course.testing.basketservice.infra.google;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.softwareskill.course.testing.basketservice.domain.delivery.Address;
import pl.softwareskill.course.testing.basketservice.testdata.AddressTestBuilder;

import java.nio.file.Files;
import java.nio.file.Paths;

import static com.natpryce.makeiteasy.MakeItEasy.make;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
class GoogleDistanceServiceTest {

    private static final String API_KEY = "any_api_key";

    @Spy
    GoogleDistanceService distanceService = new GoogleDistanceService(API_KEY);

    @Test
    void getsDistance() {
        var source = make(AddressTestBuilder.WarehouseAddress);
        var target = make(AddressTestBuilder.ShippingAddress);
        givenResponseForDistance( source, target, "/distance-response.json");

        long distance = distanceService.getDistanceFor(source, target);

        assertThat(distance)
                .isEqualTo(294605);
    }

    @SneakyThrows
    private void givenResponseForDistance(Address source, Address target, String responseFile) {
        String response = new String(Files.readAllBytes(Paths.get(getClass().getResource(responseFile).toURI())));
        doReturn(response).when(distanceService)
                .makeRequest(source, target);
    }
}
