package pl.softwareskill.course.testing.basketservice.infra.google;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import pl.softwareskill.course.testing.basketservice.testdata.AddressTestBuilder;

import static com.natpryce.makeiteasy.MakeItEasy.make;
import static org.assertj.core.api.Assertions.assertThat;

@Tag("integration")
class GoogleDistanceServiceIntegrationTest {

    private static final String API_KEY = "AIzaSyAZrcoDWoFziczVINKFkWNuJSSRToeAPls";

    @Test
    void getsDistance() {
        var distanceService = new GoogleDistanceService(API_KEY);
        var source = make(AddressTestBuilder.WarehouseAddress);
        var target = make(AddressTestBuilder.ShippingAddress);

        long distance = distanceService.getDistanceFor(source, target);

        assertThat(distance)
                .isPositive();
    }
}
