package pl.softwareskill.course.testing.basketservice.infra.google;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.VisibleForTesting;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.reactive.function.client.WebClient;
import pl.softwareskill.course.testing.basketservice.domain.delivery.Address;
import pl.softwareskill.course.testing.basketservice.domain.delivery.DistanceService;

import java.util.StringJoiner;

@RequiredArgsConstructor
public class GoogleDistanceService implements DistanceService {

    // https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=XXX&destinations=YYY&key=KEY

    private static final ObjectMapper MAPPER = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    private final String apiKey;

    @Override
    @SneakyThrows
    public long getDistanceFor(Address source, Address target) {
        String json = makeRequest(source, target);

        DistanceResponse distanceResponse = MAPPER.readValue(json, DistanceResponse.class);

        Integer distance = distanceResponse.getRows().get(0)
                .getElements().get(0)
                .getDistance()
                .getValue();

        return Long.valueOf(distance);
    }

    @VisibleForTesting
    protected String makeRequest(Address source, Address target) {
        String url = "https://maps.googleapis.com/maps/api/distancematrix/json";

        return WebClient.create(url)
                .get()
                .uri(String.format("?units=imperial&origins=%s&destinations=%s&key=%s",
                        addressParam(source), addressParam(target), apiKey))
                .retrieve()
                .bodyToMono(String.class)
                .log()
                .block();
    }

    private static String addressParam(Address address) {
        return new StringJoiner(", ")
                .add(address.getCountry())
                .add(address.getCity())
                .add(address.getStreet() + " " + address.getNumber())
                .toString();
    }
}
