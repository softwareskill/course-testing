package pl.softwareskill.course.testing.basketservice.infra.google;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.softwareskill.course.testing.basketservice.domain.delivery.DistanceService;

@Configuration
public class DistanceServiceGoogleInfraConfig {

    @Bean
    DistanceService distanceService(@Value("${app.google.apikey}") String apiKey) {
        return new GoogleDistanceService(apiKey);
    }
}
