package pl.softwareskill.course.testing.basketservice.infra.google;

import lombok.Data;

import java.util.List;

@Data
class DistanceResponse {

    private List<DestinationRow> rows;

    @Data
    public static class DestinationRow {
        private List<DestinationRowElement> elements;
    }

    @Data
    public static class DestinationRowElement {
        private String status;
        private Distance distance;
    }

    @Data
    public static class Distance {
        private String text;
        private Integer value;
    }
}
