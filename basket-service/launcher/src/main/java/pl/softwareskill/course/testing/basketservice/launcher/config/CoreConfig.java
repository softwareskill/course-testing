package pl.softwareskill.course.testing.basketservice.launcher.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import pl.softwareskill.course.testing.basketservice.rest.RestConfig;
import pl.softwareskill.course.testing.basketservice.service.DomainConfig;

@Configuration
@Import({DomainConfig.class, FakeOrderServiceConfig.class, RestConfig.class})
@Slf4j
public class CoreConfig {
}
