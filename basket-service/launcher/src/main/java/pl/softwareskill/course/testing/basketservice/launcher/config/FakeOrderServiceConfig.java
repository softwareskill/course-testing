package pl.softwareskill.course.testing.basketservice.launcher.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.softwareskill.course.testing.basketservice.domain.Basket;
import pl.softwareskill.course.testing.basketservice.domain.OrderService;

@Configuration
public class FakeOrderServiceConfig {

    @Bean
    OrderService orderService() {
        return new FakeOrderService();
    }

    @Slf4j
    static class FakeOrderService implements OrderService {

        @Override
        public String createOrder(Basket basket) {
            log.info("Creating order out of basket: {}", basket);
            return "order-" + basket.getBasketId().toString();
        }
    }
}
