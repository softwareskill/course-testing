package pl.softwareskill.course.testing.basketservice.launcher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import pl.softwareskill.course.testing.basketservice.launcher.config.CoreConfig;
import pl.softwareskill.course.testing.basketservice.launcher.config.InfraConfig;

@SpringBootApplication
@Import({CoreConfig.class, InfraConfig.class})
public class BasketApplication {

    public static void main(String[] args) {
        SpringApplication.run(BasketApplication.class, args);
    }
}
