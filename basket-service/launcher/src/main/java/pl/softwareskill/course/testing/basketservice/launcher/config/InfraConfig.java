package pl.softwareskill.course.testing.basketservice.launcher.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import pl.softwareskill.course.testing.basketservice.infra.google.DistanceServiceGoogleInfraConfig;
import pl.softwareskill.course.testing.basketservice.infra.persistence.PersistenceConfig;

@Configuration
@Import({PersistenceConfig.class, DistanceServiceGoogleInfraConfig.class})
@Slf4j
public class InfraConfig {
}
