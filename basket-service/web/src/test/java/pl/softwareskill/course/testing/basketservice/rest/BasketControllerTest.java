package pl.softwareskill.course.testing.basketservice.rest;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pl.softwareskill.course.testing.basketservice.component.config.ComponentTestConfig;
import pl.softwareskill.course.testing.basketservice.component.steps.BasketRepositorySteps;
import pl.softwareskill.course.testing.basketservice.component.steps.ProductRepositorySteps;
import pl.softwareskill.course.testing.basketservice.testdata.BasketTestBuilder;
import pl.softwareskill.course.testing.basketservice.testdata.ProductTestBuilder;

import static com.natpryce.makeiteasy.MakeItEasy.a;
import static com.natpryce.makeiteasy.MakeItEasy.make;
import static org.hamcrest.Matchers.matchesRegex;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = {RestConfig.class, ComponentTestConfig.class})
@AutoConfigureMockMvc
@Disabled("CDC tests enabled")
class BasketControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BasketRepositorySteps basketRepositorySteps;

    @Autowired
    private ProductRepositorySteps productRepositorySteps;

    @Test
    void createsBasket() throws Exception {
        this.mockMvc.perform(post("/basket"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().string(matchesRegex("\\{\"basketId\":\"(.+)\"\\}")));
    }

    @Test
    void insertsProductToBasket() throws Exception {
        var basket = make(a(BasketTestBuilder.EmptyBasket));
        basketRepositorySteps.givenWeHaveBasket(basket);

        var product = make(ProductTestBuilder.BookCleanCode);
        productRepositorySteps.givenWeHaveProduct(product);

        this.mockMvc.perform(post("/basket/{basketId}/add/{productId}", basket.getBasketId(), product.getProductId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(matchesRegex("\\{\"basketId\":\"(.+)\"\\}")));
    }

    @Test
    void getsSummaryForDeliveryAddress() throws Exception {
        var basket = make(a(BasketTestBuilder.BasketWithProducts));
        basketRepositorySteps.givenWeHaveBasket(basket);

        String body = "{\n" +
                "\t\"shippingAddress\": {\n" +
                "\t\t\"country\": \"Polska\",\n" +
                "\t\t\"city\": \"Warszawa\",\n" +
                "\t\t\"street\": \"Podwale\",\n" +
                "\t\t\"number\": \"12\"\n" +
                "\t}\n" +
                "}";
        this.mockMvc.perform(
                post("/basket/{basketId}/summary", basket.getBasketId())
                    .content(body)
                    .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalPrice").isNumber());
    }
}
