package pl.softwareskill.course.testing.basketservice.rest;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import pl.softwareskill.course.testing.basketservice.component.config.ComponentTestConfig;
import pl.softwareskill.course.testing.basketservice.component.steps.BasketRepositorySteps;
import pl.softwareskill.course.testing.basketservice.component.steps.DistanceSteps;
import pl.softwareskill.course.testing.basketservice.testdata.BasketTestBuilder;

import java.util.UUID;

import static com.natpryce.makeiteasy.MakeItEasy.*;

@SpringBootTest(classes = {RestConfig.class, ComponentTestConfig.class})
@AutoConfigureMockMvc
class BaseContractTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BasketRepositorySteps basketRepositorySteps;

    @Autowired
    private DistanceSteps distanceSteps;

    @BeforeEach
    public void setup() {
        var basket = make(a(BasketTestBuilder.BasketWithProducts)
            .but(with(BasketTestBuilder.BasketId, UUID.fromString("063b360b-854a-4f5a-9081-fd0157135d9c"))));

        basketRepositorySteps.givenWeHaveBasket(basket);

        distanceSteps.defaultResponse();

        RestAssuredMockMvc.mockMvc(mockMvc);
    }
}
