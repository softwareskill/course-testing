package pl.softwareskill.course.testing.basketservice.rest.view;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class BasketSummaryView {

    private BigDecimal totalPrice;
}
