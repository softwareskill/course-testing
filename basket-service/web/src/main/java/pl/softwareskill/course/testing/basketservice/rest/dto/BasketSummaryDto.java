package pl.softwareskill.course.testing.basketservice.rest.dto;

import lombok.Data;
import pl.softwareskill.course.testing.basketservice.rest.view.AddressView;

@Data
public class BasketSummaryDto {

    private AddressView shippingAddress;
}
