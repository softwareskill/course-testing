package pl.softwareskill.course.testing.basketservice.rest.view;

import lombok.Data;
import pl.softwareskill.course.testing.basketservice.domain.Basket;

import java.util.UUID;

@Data
public class BasketView {

    private UUID basketId;

    public static BasketView valueOf(Basket basket) {
        BasketView view = new BasketView();
        view.setBasketId(basket.getBasketId());
        return view;
    }
}
