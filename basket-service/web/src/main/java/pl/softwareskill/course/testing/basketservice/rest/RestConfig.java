package pl.softwareskill.course.testing.basketservice.rest;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import pl.softwareskill.course.testing.basketservice.service.DomainConfig;

@EnableWebMvc
@ComponentScan(basePackages = "pl.softwareskill.course.testing.basketservice.rest.controller")
@Import(DomainConfig.class)
public class RestConfig {
}
