package pl.softwareskill.course.testing.basketservice.rest.view;

import lombok.Data;
import pl.softwareskill.course.testing.basketservice.domain.delivery.Address;

@Data
public class AddressView {

    private String country;
    private String city;
    private String street;
    private String number;

    public Address toAddress() {
        return Address.builder()
                .country(country)
                .city(city)
                .street(street)
                .number(number)
                .build();
    }
}
