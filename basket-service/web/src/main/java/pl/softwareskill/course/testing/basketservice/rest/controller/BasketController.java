package pl.softwareskill.course.testing.basketservice.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.softwareskill.course.testing.basketservice.domain.Basket;
import pl.softwareskill.course.testing.basketservice.rest.dto.BasketSummaryDto;
import pl.softwareskill.course.testing.basketservice.rest.view.AddressView;
import pl.softwareskill.course.testing.basketservice.rest.view.BasketSummaryView;
import pl.softwareskill.course.testing.basketservice.rest.view.BasketView;
import pl.softwareskill.course.testing.basketservice.service.BasketPriceService;
import pl.softwareskill.course.testing.basketservice.service.BasketService;

import java.math.BigDecimal;
import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping(path = "/basket")
@RequiredArgsConstructor
public class BasketController {

    private final BasketService basketService;
    private final BasketPriceService basketPriceService;

    @PostMapping
    public ResponseEntity<BasketView> createBasket() {
        Basket basket = basketService.createBasket();

        return ResponseEntity.created(URI.create("/" + basket.getBasketId() + "/summary"))
                .body(BasketView.valueOf(basket));
    }

    @PostMapping("/{basketId}/add/{productId}")
    public ResponseEntity<BasketView> addProduct(@PathVariable UUID basketId,
                                                 @PathVariable UUID productId) {
        try {
            Basket basket = basketService.addItem(basketId, productId);

            return ResponseEntity.ok(BasketView.valueOf(basket));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/{basketId}/summary")
    public ResponseEntity<BasketSummaryView> summary(@PathVariable UUID basketId,
                                                     @RequestBody BasketSummaryDto basketSummaryView) {
        try {
            AddressView shippingAddress = basketSummaryView.getShippingAddress();
            BigDecimal summaryPrice = basketPriceService.getBasketSummaryPrice(basketId, shippingAddress.toAddress());

            return ResponseEntity.ok(BasketSummaryView.builder()
                    .totalPrice(summaryPrice)
                    .build());
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
