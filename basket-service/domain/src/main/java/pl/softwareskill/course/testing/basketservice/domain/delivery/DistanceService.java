package pl.softwareskill.course.testing.basketservice.domain.delivery;

public interface DistanceService {

    /**
     * Returns distance in meters.
     *
     * @param source
     * @param target
     * @return distance in meters
     */
    long getDistanceFor(Address source, Address target);
}
