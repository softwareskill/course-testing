package pl.softwareskill.course.testing.basketservice.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(indexes = @Index(name = "product_id_idx", columnList = "productId"))
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@EqualsAndHashCode
@ToString
public class Product {

    @Id
    @Column(name = "productId", nullable = false, updatable = false)
    UUID productId;
    String name;
    BigDecimal price;
    boolean available;
}
