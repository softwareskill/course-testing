package pl.softwareskill.course.testing.basketservice.domain;

import java.math.BigDecimal;

public interface DiscountPolicy {

    BigDecimal getDiscount(Basket basket);
}
