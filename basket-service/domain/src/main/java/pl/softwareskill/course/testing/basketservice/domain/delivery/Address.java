package pl.softwareskill.course.testing.basketservice.domain.delivery;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Address {

    String country;
    String city;
    String street;
    String number;
}
