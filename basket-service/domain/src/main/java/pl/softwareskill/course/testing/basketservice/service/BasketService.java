package pl.softwareskill.course.testing.basketservice.service;

import lombok.RequiredArgsConstructor;
import pl.softwareskill.course.testing.basketservice.domain.Basket;
import pl.softwareskill.course.testing.basketservice.domain.BasketRepository;
import pl.softwareskill.course.testing.basketservice.domain.ProductRepository;

import java.util.UUID;

@RequiredArgsConstructor
public class BasketService {

    private final BasketRepository basketRepository;
    private final ProductRepository productRepository;

    public Basket createBasket() {
        var newBasket = new Basket();
        basketRepository.save(newBasket);
        return newBasket;
    }

    public Basket addItem(UUID basketId, UUID productId) {
        var basket = basketRepository.getById(basketId)
                .orElseThrow(IllegalArgumentException::new);

        var product = productRepository.getById(productId)
                .orElseThrow(IllegalArgumentException::new);

        basket.insert(product);

        return basket;
    }
}
