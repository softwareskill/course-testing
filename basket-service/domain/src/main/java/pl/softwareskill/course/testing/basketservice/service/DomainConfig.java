package pl.softwareskill.course.testing.basketservice.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.softwareskill.course.testing.basketservice.domain.BasketRepository;
import pl.softwareskill.course.testing.basketservice.domain.OrderService;
import pl.softwareskill.course.testing.basketservice.domain.ProductRepository;
import pl.softwareskill.course.testing.basketservice.domain.delivery.Address;
import pl.softwareskill.course.testing.basketservice.domain.delivery.DeliveryCostCalculator;
import pl.softwareskill.course.testing.basketservice.domain.delivery.DistanceDeliveryCostCalculator;
import pl.softwareskill.course.testing.basketservice.domain.delivery.DistanceService;

import java.math.BigDecimal;

@Configuration
public class DomainConfig {

    @Bean
    BasketService basketService(BasketRepository basketRepository, ProductRepository productRepository) {
        return new BasketService(basketRepository, productRepository);
    }

    @Bean
    BasketPriceService basketPriceService(BasketRepository basketRepository, DeliveryCostCalculator deliveryCostCalculator) {
        return new BasketPriceService(basketRepository, deliveryCostCalculator);
    }

    @Bean
    DeliveryCostCalculator deliveryCostCalculator(DistanceService distanceService, Address warehouseAddress,
              @Value("${app.delivery.price.base}") BigDecimal basePrice,
              @Value("${app.delivery.price.perKm}") BigDecimal pricePerKm,
              @Value("${app.delivery.freeShippingDistanceKm}") long freeShippingDistanceKm,
              @Value("${app.delivery.basePriceShippingDistanceKm}") long basePriceShippingDistanceKm
    ) {
        return new DistanceDeliveryCostCalculator(distanceService, warehouseAddress, basePrice, pricePerKm,
                freeShippingDistanceKm, basePriceShippingDistanceKm);
    }

    @Bean
    Address warehouseAddress(
            @Value("${app.delivery.warehouse.address.country}") String country,
            @Value("${app.delivery.warehouse.address.city}") String city,
            @Value("${app.delivery.warehouse.address.street}") String street,
            @Value("${app.delivery.warehouse.address.number}") String number
    ) {
        return Address.builder()
                .country(country)
                .city(city)
                .street(street)
                .number(number)
                .build();
    }

    @Bean
    CreateOrderService createOrderService(BasketRepository basketRepository, OrderService orderService) {
        return new CreateOrderService(basketRepository, orderService, new OrderCreationPreconditions());
    }
}
