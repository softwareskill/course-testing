package pl.softwareskill.course.testing.basketservice.service;

import lombok.RequiredArgsConstructor;
import pl.softwareskill.course.testing.basketservice.domain.Basket;
import pl.softwareskill.course.testing.basketservice.domain.BasketRepository;
import pl.softwareskill.course.testing.basketservice.domain.OrderService;

import java.util.UUID;

@RequiredArgsConstructor
public class CreateOrderService {

    private final BasketRepository basketRepository;
    private final OrderService orderService;
    private final OrderCreationPreconditions orderCreationPreconditions;

    public String createOrder(UUID basketId) {
        Basket basket = basketRepository.getById(basketId)
                .orElseThrow(this::onBasketNotFound);

        checkBasket(basket);

        return orderService.createOrder(basket);
    }

    private RuntimeException onBasketNotFound() {
        return new IllegalArgumentException("Basket not found.");
    }

    private void checkBasket(Basket basket) {
        if (orderCreationPreconditions.emptyBasket(basket)) {
            throw new IllegalStateException("Cannot create order from empty basket.");
        }
        if (!orderCreationPreconditions.allProductsAreAvailable(basket)) {
            throw new IllegalStateException("Some product is not available.");
        }
    }
}
