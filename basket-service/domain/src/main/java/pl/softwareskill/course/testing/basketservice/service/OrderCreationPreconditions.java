package pl.softwareskill.course.testing.basketservice.service;

import pl.softwareskill.course.testing.basketservice.domain.Basket;
import pl.softwareskill.course.testing.basketservice.domain.Product;

class OrderCreationPreconditions {

    boolean emptyBasket(Basket basket) {
        return basket.getInsertedProducts().isEmpty();
    }

    boolean allProductsAreAvailable(Basket basket) {
        return basket.getInsertedProducts()
                .stream()
                .allMatch(Product::isAvailable);
    }
}
