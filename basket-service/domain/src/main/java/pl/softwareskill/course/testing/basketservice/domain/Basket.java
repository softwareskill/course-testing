package pl.softwareskill.course.testing.basketservice.domain;

import jakarta.persistence.*;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import static jakarta.persistence.FetchType.EAGER;
import static java.util.Objects.requireNonNull;

@Entity
@Table(indexes = @Index(name = "basket_id_idx", columnList = "basketId"))
@ToString
public class Basket {

    @Id
    @Column(name = "basketId", nullable = false, updatable = false)
    private final UUID basketId;

    @ManyToMany(fetch = EAGER)
    @JoinTable(name = "basket_products",
            joinColumns = @JoinColumn(name = "basket_id", foreignKey = @ForeignKey(name = "basket")),
            inverseJoinColumns = @JoinColumn(name = "product_id", foreignKey = @ForeignKey(name = "product"))
    )
    private final Collection<Product> products;

    public Basket() {
        this(UUID.randomUUID());
    }

    public Basket(UUID basketId) {
        this.basketId = basketId;
        this.products = new ArrayList<>();
    }

    public UUID getBasketId() {
        return basketId;
    }

    public void insert(Product product) {
        requireNonNull(product);
        products.add(product);
    }

    public Collection<Product> getInsertedProducts() {
        return new ArrayList<>(products);
    }

    public BigDecimal getItemsTotalPrice() {
        return getInsertedProducts()
                .stream()
                .map(Product::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal getFinalPrice(DiscountPolicy discountPolicy) {
        BigDecimal totalPrice = getItemsTotalPrice();
        BigDecimal discount = discountPolicy.getDiscount(this);

        return totalPrice.subtract(discount);
    }
}
