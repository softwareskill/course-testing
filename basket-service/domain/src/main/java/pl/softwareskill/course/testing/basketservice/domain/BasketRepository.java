package pl.softwareskill.course.testing.basketservice.domain;

import java.util.Optional;
import java.util.UUID;

public interface BasketRepository {

    Optional<Basket> getById(UUID basketId);

    void save(Basket basket);
}
