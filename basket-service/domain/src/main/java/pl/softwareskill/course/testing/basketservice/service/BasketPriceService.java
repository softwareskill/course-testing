package pl.softwareskill.course.testing.basketservice.service;

import lombok.RequiredArgsConstructor;
import pl.softwareskill.course.testing.basketservice.domain.BasketRepository;
import pl.softwareskill.course.testing.basketservice.domain.delivery.Address;
import pl.softwareskill.course.testing.basketservice.domain.delivery.DeliveryCostCalculator;

import java.math.BigDecimal;
import java.util.UUID;

@RequiredArgsConstructor
public class BasketPriceService {

    private final BasketRepository basketRepository;
    private final DeliveryCostCalculator deliveryCostCalculator;

    public BigDecimal getBasketSummaryPrice(UUID basketId, Address deliveryAddress) {
        var basket = basketRepository.getById(basketId)
                .orElseThrow(IllegalArgumentException::new);

        var totalPrice = basket.getItemsTotalPrice();
        var deliveryCost = deliveryCostCalculator.calculateDeliveryCost(basket, deliveryAddress);

        return totalPrice.add(deliveryCost);
    }
}
