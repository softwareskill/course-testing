package pl.softwareskill.course.testing.basketservice.domain;

public interface OrderService {

    String createOrder(Basket basket);
}
