package pl.softwareskill.course.testing.basketservice.domain.delivery;

import pl.softwareskill.course.testing.basketservice.domain.Basket;

import java.math.BigDecimal;

public interface DeliveryCostCalculator {

    BigDecimal calculateDeliveryCost(Basket basket, Address address);
}
