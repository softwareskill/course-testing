package pl.softwareskill.course.testing.basketservice.domain;

import java.util.Optional;
import java.util.UUID;

public interface ProductRepository {

    Optional<Product> getById(UUID productId);

    void save(Product product);
}
