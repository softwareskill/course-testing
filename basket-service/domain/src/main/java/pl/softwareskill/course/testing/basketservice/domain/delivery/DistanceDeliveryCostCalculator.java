package pl.softwareskill.course.testing.basketservice.domain.delivery;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.testing.basketservice.domain.Basket;

import java.math.BigDecimal;

@RequiredArgsConstructor
@Slf4j
public class DistanceDeliveryCostCalculator implements DeliveryCostCalculator {

    private static final long ONE_KILOMETER = 1000;

    private final DistanceService distanceService;
    private final Address sourceAddress;
    private final BigDecimal basePrice;
    private final BigDecimal pricePerKm;
    private final long freeShippingDistanceKm;
    private final long basePriceShippingDistanceKm;

    @Override
    public BigDecimal calculateDeliveryCost(Basket basket, Address address) {
        var distance = getDistance(address);
        var distanceKm = distance / ONE_KILOMETER;
        return calculateShippingPrice(distanceKm);
    }

    private long getDistance(Address address) {
        try {
            return distanceService.getDistanceFor(sourceAddress, address);
        } catch (Exception e) {
            var defaultDistance = basePriceShippingDistanceKm;
            log.warn("Getting distance failed. Assuming default distance: {}", defaultDistance, e);
            return defaultDistance;
        }
    }

    private BigDecimal calculateShippingPrice(long distanceKm) {
        if (distanceKm <= freeShippingDistanceKm) {
            return BigDecimal.ZERO;
        }

        if (distanceKm <= basePriceShippingDistanceKm) {
            return basePrice;
        }

        var additionalDistance = distanceKm - basePriceShippingDistanceKm;
        return basePrice.add(pricePerKm.multiply(BigDecimal.valueOf(additionalDistance)));
    }
}
