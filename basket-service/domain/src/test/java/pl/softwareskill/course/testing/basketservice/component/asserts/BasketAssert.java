package pl.softwareskill.course.testing.basketservice.component.asserts;

import lombok.RequiredArgsConstructor;
import pl.softwareskill.course.testing.basketservice.domain.Basket;
import pl.softwareskill.course.testing.basketservice.domain.Product;

import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@RequiredArgsConstructor(staticName = "assertThatBasket")
public class BasketAssert {

    private final Basket basket;

    public BasketAssert isEmpty() {
        assertThat(basket.getInsertedProducts())
                .isEmpty();

        return this;
    }

    public BasketAssert hasProduct(Product product) {
        assertThat(basket.getInsertedProducts())
                .anyMatch(p -> Objects.equals(p, product));

        return this;
    }
}
