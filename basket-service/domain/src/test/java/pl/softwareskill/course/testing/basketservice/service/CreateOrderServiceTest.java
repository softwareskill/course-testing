package pl.softwareskill.course.testing.basketservice.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.softwareskill.course.testing.basketservice.domain.Basket;
import pl.softwareskill.course.testing.basketservice.domain.BasketRepository;
import pl.softwareskill.course.testing.basketservice.domain.OrderService;
import pl.softwareskill.course.testing.basketservice.testdata.ProductTestBuilder;

import java.util.Optional;

import static com.natpryce.makeiteasy.MakeItEasy.*;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static pl.softwareskill.course.testing.basketservice.testdata.BasketTestBuilder.*;

@ExtendWith(MockitoExtension.class)
class CreateOrderServiceTest {

    @Mock
    BasketRepository basketRepository;
    @Mock
    OrderService orderService;

    CreateOrderService createOrderService;

    @BeforeEach
    void setup() {
        createOrderService = new CreateOrderService(basketRepository, orderService, new OrderCreationPreconditions());
    }

    @Test
    @DisplayName("Creates order for basket")
    void createsOrderForBasket() {
        // given
        var basket = make(a(BasketWithProducts));
        givenBasketInRepository(basket);

        // when
        createOrderService.createOrder(basket.getBasketId());

        // then
        then(orderService)
                .should()
                .createOrder(basket);
    }

    @Test
    @DisplayName("Does not create order for not existing basket")
    void doesNotCreateOrderForNonExistingBasket() {
        // given
        var basket = make(a(BasketWithProducts));
        given(basketRepository.getById(basket.getBasketId())).willReturn(Optional.empty());

        // when
        assertThatCode(() -> createOrderService.createOrder(basket.getBasketId()))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Basket not found.");

        // then
        then(orderService)
                .should(never())
                .createOrder(basket);
    }

    @Test
    @DisplayName("Does not create order for empty basket")
    void doesNotCreateOrderForEmptyBasket() {
        // given
        var basket = make(a(EmptyBasket));
        givenBasketInRepository(basket);

        // when
        assertThatCode(() -> createOrderService.createOrder(basket.getBasketId()))
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("Cannot create order from empty basket.");

        // then
        then(orderService)
                .should(never())
                .createOrder(basket);
    }

    @Test
    @DisplayName("Does not create order for basket with not active product")
    void doesNotCreateOrderForBasketWithNotActiveProduct() {
        // given
        var basket = make(a(BasketWithProducts)
                .but(with(Products, asList(
                        make(a(ProductTestBuilder.BasicProduct)),
                        make(ProductTestBuilder.UnavailableProduct)
                ))));
        givenBasketInRepository(basket);

        // when
        assertThatCode(() -> createOrderService.createOrder(basket.getBasketId()))
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("Some product is not available.");

        // then
        then(orderService)
                .should(never())
                .createOrder(basket);
    }

    private void givenBasketInRepository(Basket basket) {
        given(basketRepository.getById(basket.getBasketId())).willReturn(Optional.of(basket));
    }
}
