package pl.softwareskill.course.testing.basketservice.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static com.natpryce.makeiteasy.MakeItEasy.*;
import static java.math.BigDecimal.ZERO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static pl.softwareskill.course.testing.basketservice.testdata.BasketTestBuilder.BasketWithProducts;
import static pl.softwareskill.course.testing.basketservice.testdata.BasketTestBuilder.EmptyBasket;
import static pl.softwareskill.course.testing.basketservice.testdata.ProductTestBuilder.*;

class BasketTest {

    @Nested
    class EmptyBasket {

        @Test
        @DisplayName("Inspects empty basket")
        void inspectsEmptyBasket() {
            // given
            var basket = make(an(EmptyBasket));

            // then
            assertThat(basket.getInsertedProducts())
                    .as("Does not contain any product.")
                    .isEmpty();
        }
    }

    @Nested
    class InsertingProduct {
        @Test
        @DisplayName("Inserts single product into empty basket")
        void insertsSingleProductIntoEmptyBasket() {
            // given
            var basket = make(an(EmptyBasket));
            var product = make(a(BasicProduct));

            // when
            basket.insert(product);

            // then
            assertThat(basket.getInsertedProducts())
                    .as("Contains only inserted product.")
                    .hasSize(1)
                    .containsExactly(product);
        }

        @Test
        @DisplayName("Inserts same product twice into empty basket")
        void insertsSameProductTwiceIntoEmptyBasket() {
            // given
            var basket = make(an(EmptyBasket));
            var anyProduct1 = make(a(BasicProduct));
            var anyProduct2 = make(a(BasicProduct));

            // when
            basket.insert(anyProduct1);
            basket.insert(anyProduct2);

            // then
            assertThat(basket.getInsertedProducts())
                    .as("Contains only inserted products.")
                    .hasSize(2)
                    .containsExactly(anyProduct1, anyProduct2);
        }
    }

    @Nested
    class PriceCalculation {

        @Test
        @DisplayName("Empty basket has zero price")
        void calculatesZeroPrice() {
            // given
            var basket = make(an(EmptyBasket));

            // then
            assertThat(basket.getItemsTotalPrice())
                    .isZero();
        }

        @Test
        @DisplayName("Calculates summary products price")
        void calculatesSummaryProductsPrice() {
            // given
            var basket = make(an(EmptyBasket));

            var product1 = make(BookCleanCode);
            var product2 = make(BookDDD);

            // when
            basket.insert(product1);
            basket.insert(product2);

            // then
            assertThat(basket.getItemsTotalPrice())
                    .isEqualTo(product1.getPrice().add(product2.getPrice()));
        }
    }

    @Nested
    @ExtendWith(MockitoExtension.class)
    class FinalPriceCalculation {

        private final BigDecimal ANY_DISCOUNT = BigDecimal.valueOf(10);

        @Mock
        DiscountPolicy discountPolicy;

        @Test
        @DisplayName("Calculates final price including discount")
        void calculatesFinalPriceIncludingDiscount() {
            // given
            var basket = make(an(BasketWithProducts));
            given(discountPolicy.getDiscount(basket)).willReturn(ANY_DISCOUNT);

            // then
            assertThat(basket.getFinalPrice(discountPolicy))
                    .isEqualTo(basket.getItemsTotalPrice().subtract(ANY_DISCOUNT));
        }

        @Test
        @DisplayName("Calculates final price without discount")
        void calculatesFinalPriceWithoutDiscount() {
            // given
            var basket = make(an(BasketWithProducts));
            given(discountPolicy.getDiscount(basket)).willReturn(ZERO);

            // then
            assertThat(basket.getFinalPrice(discountPolicy))
                    .isEqualTo(basket.getItemsTotalPrice());
        }
    }
}
