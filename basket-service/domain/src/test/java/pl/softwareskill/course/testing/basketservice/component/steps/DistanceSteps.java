package pl.softwareskill.course.testing.basketservice.component.steps;

import lombok.RequiredArgsConstructor;
import pl.softwareskill.course.testing.basketservice.domain.delivery.Address;
import pl.softwareskill.course.testing.basketservice.domain.delivery.DistanceService;

import jakarta.annotation.PostConstruct;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@RequiredArgsConstructor
public class DistanceSteps {

    private static final Long DEFAULT_DISTANCE = 10_000L;

    private final DistanceService distanceService;
    private final Address warehouseAddress;

    public void givenWeHaveDistance(Address target, Long distance) {
        given(distanceService.getDistanceFor(warehouseAddress, target)).willReturn(distance);
    }

    @PostConstruct
    public void defaultResponse() {
        given(distanceService.getDistanceFor(any(Address.class), any(Address.class)))
                .willReturn(DEFAULT_DISTANCE);
    }
}
