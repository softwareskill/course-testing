package pl.softwareskill.course.testing.basketservice.component;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import pl.softwareskill.course.testing.basketservice.component.config.ComponentTestConfig;
import pl.softwareskill.course.testing.basketservice.component.steps.BasketRepositorySteps;
import pl.softwareskill.course.testing.basketservice.component.steps.DistanceSteps;
import pl.softwareskill.course.testing.basketservice.service.BasketPriceService;
import pl.softwareskill.course.testing.basketservice.service.DomainConfig;
import pl.softwareskill.course.testing.basketservice.testdata.AddressTestBuilder;
import pl.softwareskill.course.testing.basketservice.testdata.BasketTestBuilder;

import java.math.BigDecimal;

import static com.natpryce.makeiteasy.MakeItEasy.a;
import static com.natpryce.makeiteasy.MakeItEasy.make;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ContextConfiguration(classes = {DomainConfig.class, ComponentTestConfig.class})
public class BasketPriceComponentTest {

    private static final long FAR_DISTANCE = 300_000L;
    public static final int SHIPPING_BASE_PRICE = 10;
    public static final int SHIPPING_PRICE_FOR_ADDITIONAL_KM = 20;

    @Autowired
    private BasketPriceService basketPriceService;

    @Autowired
    private BasketRepositorySteps basketRepositorySteps;

    @Autowired
    private DistanceSteps distanceSteps;

    @Test
    void calculatesPriceForBasket() {
        // given
        var basket = make(a(BasketTestBuilder.BasketWithProducts));
        basketRepositorySteps.givenWeHaveBasket(basket);

        var address = make(AddressTestBuilder.ShippingAddress);
        distanceSteps.givenWeHaveDistance(address, FAR_DISTANCE);

        // when
        BigDecimal summaryPrice = basketPriceService.getBasketSummaryPrice(basket.getBasketId(), address);

        // then
        assertThat(summaryPrice)
                .isEqualTo(basket.getItemsTotalPrice()
                        .add(BigDecimal.valueOf(SHIPPING_BASE_PRICE))
                        .add(BigDecimal.valueOf(SHIPPING_PRICE_FOR_ADDITIONAL_KM)));
    }
}
