package pl.softwareskill.course.testing.basketservice.domain.delivery;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.softwareskill.course.testing.basketservice.domain.Basket;
import pl.softwareskill.course.testing.basketservice.testdata.AddressTestBuilder;
import pl.softwareskill.course.testing.basketservice.testdata.BasketTestBuilder;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static com.natpryce.makeiteasy.MakeItEasy.a;
import static com.natpryce.makeiteasy.MakeItEasy.make;
import static java.math.BigDecimal.ZERO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class DistanceDeliveryCostCalculatorTest {

    private static final BigDecimal BASE_PRICE = BigDecimal.valueOf(10);
    private static final BigDecimal PRICE_PER_KM = BigDecimal.valueOf(0.1);
    private static final long FREE_SHIPPING_DISTANCE_KM = 10;
    private static final long BASE_PRICE_SHIPPING_DISTANCE_KM = 100;
    private static final Basket ANY_BASKET = make(a(BasketTestBuilder.BasketWithProducts));
    private static final Address WAREHOUSE_ADDRESS = make(AddressTestBuilder.WarehouseAddress);
    private static final Address ANY_ADDRESS = make(AddressTestBuilder.ShippingAddress);

    @Mock
    DistanceService distanceService;

    @DisplayName("Calculates shipping price")
    @ParameterizedTest(name = "Calculates {2}")
    @MethodSource("cases")
    void calculatesPriceForShipping(long distanceKm, BigDecimal expectedPrice, String name) {
        // given
        given(distanceService.getDistanceFor(WAREHOUSE_ADDRESS, ANY_ADDRESS)).willReturn(distanceKm * 1000);
        var calculator = new DistanceDeliveryCostCalculator(distanceService, WAREHOUSE_ADDRESS,
                BASE_PRICE, PRICE_PER_KM, FREE_SHIPPING_DISTANCE_KM, BASE_PRICE_SHIPPING_DISTANCE_KM);

        // when
        var result = calculator.calculateDeliveryCost(ANY_BASKET, ANY_ADDRESS);

        // then
        assertThat(result)
                .isEqualByComparingTo(expectedPrice);
    }

    static Stream<Arguments> cases() {
        return Stream.of(
                Arguments.of(FREE_SHIPPING_DISTANCE_KM - 1,
                        ZERO, "Free shipping under 10km"),

                Arguments.of(FREE_SHIPPING_DISTANCE_KM,
                        ZERO, "Free shipping for maximum free distance"),

                Arguments.of(FREE_SHIPPING_DISTANCE_KM + 1,
                        BASE_PRICE, "Base shipping price above free distance"),

                Arguments.of(BASE_PRICE_SHIPPING_DISTANCE_KM,
                        BASE_PRICE, "Base shipping price maximum base shipping distance"),

                Arguments.of(BASE_PRICE_SHIPPING_DISTANCE_KM + 50,
                        BASE_PRICE.add(BigDecimal.valueOf(5)), "Fee per kilometer above base shipping distance")
        );
    }
}