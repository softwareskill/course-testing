package pl.softwareskill.course.testing.basketservice.component.steps;

import lombok.RequiredArgsConstructor;
import pl.softwareskill.course.testing.basketservice.domain.Basket;
import pl.softwareskill.course.testing.basketservice.domain.BasketRepository;

import java.util.Optional;

import static org.mockito.BDDMockito.given;

@RequiredArgsConstructor
public class BasketRepositorySteps {

    private final BasketRepository basketRepository;

    public void givenWeHaveBasket(Basket basket) {
        given(basketRepository.getById(basket.getBasketId())).willReturn(Optional.of(basket));
    }
}
