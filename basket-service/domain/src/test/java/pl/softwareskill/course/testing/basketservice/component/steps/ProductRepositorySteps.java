package pl.softwareskill.course.testing.basketservice.component.steps;

import lombok.RequiredArgsConstructor;
import pl.softwareskill.course.testing.basketservice.domain.Product;
import pl.softwareskill.course.testing.basketservice.domain.ProductRepository;

import java.util.Optional;

import static org.mockito.BDDMockito.given;

@RequiredArgsConstructor
public class ProductRepositorySteps {

    private final ProductRepository productRepository;

    public void givenWeHaveProduct(Product product) {
        given(productRepository.getById(product.getProductId())).willReturn(Optional.of(product));
    }
}
