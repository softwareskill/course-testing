package pl.softwareskill.course.testing.basketservice.testdata;

import com.natpryce.makeiteasy.Instantiator;
import com.natpryce.makeiteasy.Maker;
import com.natpryce.makeiteasy.Property;
import pl.softwareskill.course.testing.basketservice.domain.Product;

import java.math.BigDecimal;
import java.util.UUID;

import static com.natpryce.makeiteasy.MakeItEasy.a;
import static com.natpryce.makeiteasy.MakeItEasy.with;
import static com.natpryce.makeiteasy.Property.newProperty;

public class ProductTestBuilder {

    public static UUID PRODUCT_ID = UUID.fromString("8f59c8b6-a439-45df-9a8f-0980b9523d03");

    public static final Property<Product, UUID> ProductId = newProperty();
    public static final Property<Product, String> Name = newProperty();
    public static final Property<Product, BigDecimal> Price = newProperty();
    public static final Property<Product, Boolean> Available = newProperty();

    public static final Instantiator<Product> BasicProduct = (lookup) -> {
        return Product.builder()
                .productId(lookup.valueOf(ProductId, PRODUCT_ID))
                .name(lookup.valueOf(Name, "Item"))
                .price(lookup.valueOf(Price, BigDecimal.TEN))
                .available(lookup.valueOf(Available, true))
                .build();
    };

    public static final Maker<Product> UnavailableProduct = a(BasicProduct)
            .but(with(Available, false));

    public static final Maker<Product> BookDDD = a(BasicProduct)
            .but(with(ProductId, UUID.fromString("c68698f4-4e9a-4302-a61c-efb78c6aff8d")))
            .but(with(Name, "Domain-Driven Design"))
            .but(with(Price, new BigDecimal("24.90")));

    public static final Maker<Product> BookCleanCode = a(BasicProduct)
            .but(with(ProductId, UUID.fromString("1d67f74b-c1e3-4b36-8eaf-a26a07d88540")))
            .but(with(Name, "Clean Code"))
            .but(with(Price, new BigDecimal("29.90")));
}
