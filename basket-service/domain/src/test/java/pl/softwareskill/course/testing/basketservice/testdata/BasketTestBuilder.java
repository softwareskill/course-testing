package pl.softwareskill.course.testing.basketservice.testdata;

import com.natpryce.makeiteasy.Instantiator;
import com.natpryce.makeiteasy.Property;
import pl.softwareskill.course.testing.basketservice.domain.Basket;
import pl.softwareskill.course.testing.basketservice.domain.Product;

import java.util.Collection;
import java.util.UUID;

import static com.natpryce.makeiteasy.MakeItEasy.make;
import static com.natpryce.makeiteasy.Property.newProperty;
import static java.util.Arrays.asList;

public class BasketTestBuilder {

    public static final Property<Basket, UUID> BasketId = newProperty();
    public static final Property<Basket, Collection<Product>> Products = newProperty();

    public static Instantiator<Basket> EmptyBasket = (lookup) -> new Basket();

    public static Instantiator<Basket> BasketWithProducts = (lookup) -> {
        var basketId = lookup.valueOf(BasketId, UUID.randomUUID());
        var basket = new Basket(basketId);

        lookup.valueOf(Products, defaultProducts()).stream()
                .forEach(basket::insert);

        return basket;
    };

    private static Collection<Product> defaultProducts() {
        return asList(
                make(ProductTestBuilder.BookCleanCode),
                make(ProductTestBuilder.BookDDD)
        );
    }
}
