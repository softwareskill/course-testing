package pl.softwareskill.course.testing.basketservice.testdata;

import com.natpryce.makeiteasy.Instantiator;
import com.natpryce.makeiteasy.Maker;
import com.natpryce.makeiteasy.Property;
import pl.softwareskill.course.testing.basketservice.domain.Product;
import pl.softwareskill.course.testing.basketservice.domain.delivery.Address;

import static com.natpryce.makeiteasy.MakeItEasy.a;
import static com.natpryce.makeiteasy.MakeItEasy.with;
import static com.natpryce.makeiteasy.Property.newProperty;

public class AddressTestBuilder {

    public static final Property<Address, String> Country = newProperty();
    public static final Property<Address, String> City = newProperty();
    public static final Property<Address, String> Street = newProperty();
    public static final Property<Address, String> Number = newProperty();

    public static final Instantiator<Address> BasicAddress = (lookup) -> {
        return Address.builder()
                .country(lookup.valueOf(Country, "Polska"))
                .city(lookup.valueOf(City, "Kraków"))
                .street(lookup.valueOf(Street, "Powiśle"))
                .number(lookup.valueOf(Number, "12"))
                .build();
    };

    public static final Maker<Address> ShippingAddress = a(BasicAddress);

    public static final Maker<Address> WarehouseAddress = a(BasicAddress)
            .but(with(City, "Warszawa"))
            .but(with(Street, "Złota"))
            .but(with(Number, "59"));
}
