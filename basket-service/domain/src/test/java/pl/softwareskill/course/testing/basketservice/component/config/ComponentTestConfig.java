package pl.softwareskill.course.testing.basketservice.component.config;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import pl.softwareskill.course.testing.basketservice.component.steps.BasketRepositorySteps;
import pl.softwareskill.course.testing.basketservice.component.steps.DistanceSteps;
import pl.softwareskill.course.testing.basketservice.component.steps.ProductRepositorySteps;
import pl.softwareskill.course.testing.basketservice.domain.BasketRepository;
import pl.softwareskill.course.testing.basketservice.domain.ProductRepository;
import pl.softwareskill.course.testing.basketservice.domain.delivery.DistanceService;
import pl.softwareskill.course.testing.basketservice.testdata.AddressTestBuilder;

import static com.natpryce.makeiteasy.MakeItEasy.make;

@Configuration
@Import(FakeOrderServiceConfig.class)
public class ComponentTestConfig {

    @MockBean
    BasketRepository basketRepository;

    @MockBean
    ProductRepository productRepository;

    @MockBean
    DistanceService distanceService;

    @Bean
    BasketRepositorySteps basketRepositorySteps() {
        return new BasketRepositorySteps(basketRepository);
    }

    @Bean
    ProductRepositorySteps productRepositorySteps() {
        return new ProductRepositorySteps(productRepository);
    }

    @Bean
    DistanceSteps distanceSteps() {
        var warehouseAddress = make(AddressTestBuilder.WarehouseAddress);
        return new DistanceSteps(distanceService, warehouseAddress);
    }
}
