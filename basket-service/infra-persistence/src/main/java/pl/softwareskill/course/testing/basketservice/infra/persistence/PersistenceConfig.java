package pl.softwareskill.course.testing.basketservice.infra.persistence;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import pl.softwareskill.course.testing.basketservice.domain.BasketRepository;
import pl.softwareskill.course.testing.basketservice.domain.ProductRepository;

@EnableJpaRepositories
@EntityScan("pl.softwareskill.course.testing.basketservice.domain")
public class PersistenceConfig {

    @Bean
    BasketRepository basketRepository(BasketJpaRepository basketJpaRepository) {
        return new BasketRepositoryJpaImpl(basketJpaRepository);
    }

    @Bean
    ProductRepository productRepository(ProductJpaRepository productJpaRepository) {
        return new ProductRepositoryJpaImpl(productJpaRepository);
    }
}
