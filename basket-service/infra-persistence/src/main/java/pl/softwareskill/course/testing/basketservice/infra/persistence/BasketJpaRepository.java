package pl.softwareskill.course.testing.basketservice.infra.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.softwareskill.course.testing.basketservice.domain.Basket;

import java.util.UUID;

interface BasketJpaRepository extends JpaRepository<Basket, UUID> {
}
