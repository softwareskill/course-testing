package pl.softwareskill.course.testing.basketservice.infra.persistence;

import lombok.RequiredArgsConstructor;
import pl.softwareskill.course.testing.basketservice.domain.Basket;
import pl.softwareskill.course.testing.basketservice.domain.BasketRepository;

import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
class BasketRepositoryJpaImpl implements BasketRepository {

    private final BasketJpaRepository basketRepository;

    @Override
    public Optional<Basket> getById(UUID basketId) {
        return basketRepository.findById(basketId);
    }

    @Override
    public void save(Basket basket) {
        basketRepository.save(basket);
    }
}
