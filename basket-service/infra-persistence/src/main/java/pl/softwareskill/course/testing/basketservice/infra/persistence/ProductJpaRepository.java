package pl.softwareskill.course.testing.basketservice.infra.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.softwareskill.course.testing.basketservice.domain.Product;

import java.util.UUID;

interface ProductJpaRepository extends JpaRepository<Product, UUID> {
}
