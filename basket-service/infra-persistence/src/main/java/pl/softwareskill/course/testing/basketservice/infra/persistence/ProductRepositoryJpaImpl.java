package pl.softwareskill.course.testing.basketservice.infra.persistence;

import lombok.RequiredArgsConstructor;
import pl.softwareskill.course.testing.basketservice.domain.Product;
import pl.softwareskill.course.testing.basketservice.domain.ProductRepository;

import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
class ProductRepositoryJpaImpl implements ProductRepository {

    private final ProductJpaRepository productRepository;

    @Override
    public Optional<Product> getById(UUID productId) {
        return productRepository.findById(productId);
    }

    @Override
    public void save(Product product) {
        productRepository.save(product);
    }
}
