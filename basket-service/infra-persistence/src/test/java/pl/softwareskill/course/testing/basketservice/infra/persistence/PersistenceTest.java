package pl.softwareskill.course.testing.basketservice.infra.persistence;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import pl.softwareskill.course.testing.basketservice.domain.BasketRepository;
import pl.softwareskill.course.testing.basketservice.domain.ProductRepository;

import static com.natpryce.makeiteasy.MakeItEasy.a;
import static com.natpryce.makeiteasy.MakeItEasy.make;
import static org.assertj.core.api.Assertions.assertThat;
import static pl.softwareskill.course.testing.basketservice.testdata.BasketTestBuilder.BasketWithProducts;

@SpringBootTest(classes = PersistenceConfig.class)
@EnableAutoConfiguration
public class PersistenceTest {

    @Autowired
    BasketRepository basketRepository;
    @Autowired
    ProductRepository productRepository;

    @Test
    void savesBasketWithoutError() {
        // given
        var basket = make(a(BasketWithProducts));

        // then
        basketRepository.save(basket);
    }

    @Test
    void findsSavedBasket() {
        // given
        var basket = make(a(BasketWithProducts));
        basketRepository.save(basket);

        // when
        var foundBasket = basketRepository.getById(basket.getBasketId());

        // then
        assertThat(foundBasket)
                .isNotEmpty();
        assertThat(foundBasket.get())
                .usingRecursiveComparison()
                .isEqualTo(basket);
    }

    @BeforeEach
    void setup() {
        var basket = make(a(BasketWithProducts));
        basket.getInsertedProducts().forEach(productRepository::save);
    }
}
