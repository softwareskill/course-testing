create table basket (
    basket_id varchar(50) not null,
    primary key (basket_id)
);

create table basket_products (
    basket_id varchar(50) not null,
    product_id varchar(50) not null
);

create table product (
    product_id varchar(50) not null,
    available boolean not null,
    name varchar(255),
    price decimal(19,2),
    primary key (product_id)
);

create index basket_id_idx on basket (basket_id);
create index product_id_idx on product (product_id);

alter table basket_products add constraint product foreign key (product_id) references product(product_id);
alter table basket_products add constraint basket foreign key (basket_id) references basket(basket_id);
