package pl.softwareskill.course.testing.examples.junit;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class E1_2_ParametrizedNamedTest {

    private static final BigDecimal BASE_PRICE = BigDecimal.valueOf(10);
    private static final BigDecimal PRICE_PER_KM = BigDecimal.valueOf(0.1);
    private static final long FREE_SHIPPING_DISTANCE_KM = 10;
    private static final long BASE_PRICE_SHIPPING_DISTANCE_KM = 100;

    @DisplayName("Calculates shipping price")
    @ParameterizedTest(name = "Calculates {2}")
    @CsvSource({
            "5, 0, Free shipping under 10km",
            "10, 0, Free shipping for maximum free distance",
            "11, 10, Base shipping price above 10km",
            "99, 10, Base shipping price maximum base shipping distance",
            "150, 15, Fee per kilometer above base shipping distance",
    })
    void calculatesPriceForShipping(long distanceKm, BigDecimal expectedPrice, String name) {
        // given
        var calculator = new ShippingPriceCalculator(BASE_PRICE, PRICE_PER_KM, FREE_SHIPPING_DISTANCE_KM, BASE_PRICE_SHIPPING_DISTANCE_KM);

        // when
        var result = calculator.calculateShippingPrice(distanceKm);

        // then
        assertThat(result)
                .isEqualByComparingTo(expectedPrice);
    }
}
