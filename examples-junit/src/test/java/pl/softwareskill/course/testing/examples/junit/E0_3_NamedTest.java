package pl.softwareskill.course.testing.examples.junit;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class E0_3_NamedTest {

    @Test
    @DisplayName("Calculates gross price")
    void grossPrice() {
        // given
        var price = BigDecimal.valueOf(100);
        var tax = BigDecimal.valueOf(23);

        // when
        var result = GrossPriceCalculator.gross(price, tax);

        // then
        assertThat(result)
                .isEqualByComparingTo(BigDecimal.valueOf(123.00));
    }
}
