package pl.softwareskill.course.testing.examples.junit;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class E0_2_SimpleShortTest {

    @Test
    void calculatesGrossPriceShort() {
        var price = BigDecimal.valueOf(100);
        var tax = BigDecimal.valueOf(23);

        assertThat(GrossPriceCalculator.gross(price, tax))
                .isEqualByComparingTo(BigDecimal.valueOf(123.00));
    }
}
