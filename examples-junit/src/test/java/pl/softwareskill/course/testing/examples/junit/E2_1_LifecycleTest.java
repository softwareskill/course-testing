package pl.softwareskill.course.testing.examples.junit;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class E2_1_LifecycleTest {

    ShippingPriceCalculator calculator;

    @BeforeEach
    void beforeEach() {
        System.out.println("Before each");

        var basePrice = BigDecimal.valueOf(10);
        var pricePerKm = BigDecimal.valueOf(0.1);
        var freeShippingDistanceKm = 10;
        var basePriceShippingDistanceKm = 100;
        calculator = new ShippingPriceCalculator(basePrice, pricePerKm, freeShippingDistanceKm, basePriceShippingDistanceKm);
    }

    @AfterEach
    void afterEach() {
        System.out.println("After each");
    }

    @BeforeAll
    static void beforeAll() {
        System.out.println("Before all");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("After all");
    }

    @DisplayName("Calculates shipping price")
    @ParameterizedTest(name = "Calculates {2}")
    @CsvSource({
            "5, 0, Free shipping under 10km",
            "10, 0, Free shipping for maximum free distance",
            "11, 10, Base shipping price above 10km",
            "99, 10, Base shipping price maximum base shipping distance",
            "150, 15, Fee per kilometer above base shipping distance",
    })
    void calculatesPriceForShipping(long distanceKm, BigDecimal expectedPrice, String name) {
        // when
        var result = calculator.calculateShippingPrice(distanceKm);

        // then
        assertThat(result)
                .isEqualByComparingTo(expectedPrice);
    }
}
