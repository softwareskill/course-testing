package pl.softwareskill.course.testing.examples.junit;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.junit.jupiter.params.provider.EnumSource.Mode.EXCLUDE;

class E1_4_EnumSourceTest {

    @ParameterizedTest(name = "Indicates physical shipping for {0}")
    @EnumSource(value = ShippingOption.class, names = {"COURIER", "INTERNAL_POST"})
    void returnsPhysicalShippingOptions(ShippingOption shippingOption) {
        // then
        assertThat(StatusMapper.isPhysicalDeliveryToAddress(shippingOption))
                .isTrue();
    }

    @ParameterizedTest(name = "Does not indicates physical shipping for {0}")
    @EnumSource(value = ShippingOption.class, names = {"COURIER", "INTERNAL_POST"}, mode = EXCLUDE)
    void returnsNotPhysicalShippingOptions(ShippingOption shippingOption) {
        // then
        assertThat(StatusMapper.isPhysicalDeliveryToAddress(shippingOption))
                .isFalse();
    }

    @ParameterizedTest(name = "This status is not covered {0}")
    @EnumSource(value = ShippingOption.class)
    void checkIfStatusIsCovered(ShippingOption shippingOption) {
        assertThat(shippingOption)
            .isIn(
                ShippingOption.ELECTRONIC_DELIVERY,
                ShippingOption.SELF_PICKUP,
                ShippingOption.COURIER,
                ShippingOption.INTERNAL_POST
            );
    }
}
