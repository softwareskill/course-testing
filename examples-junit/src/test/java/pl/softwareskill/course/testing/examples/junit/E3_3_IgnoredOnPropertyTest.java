package pl.softwareskill.course.testing.examples.junit;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfSystemProperty;

@EnabledIfSystemProperty(named = "feature.enabled", matches = "true", disabledReason = "Feature not enabled")
class E3_3_IgnoredOnPropertyTest {

    @Test
    void ignoredTest() {
        System.out.println("Wont run");
    }
}
