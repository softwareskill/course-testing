package pl.softwareskill.course.testing.examples.junit;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class E3_4_NestedTest {

    @Nested
    @DisplayName("Cases A")
    class CasesA {

        @Test
        void caseA1() {
        }

        @Test
        void caseA2() {
        }

        @Test
        void caseA3() {
        }
    }

    @Nested
    @DisplayName("Cases B")
    class CasesB {

        @Test
        void caseB1() {
        }

        @Test
        void caseB2() {
        }
    }
}
