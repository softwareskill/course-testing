package pl.softwareskill.course.testing.examples.junit;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Disabled
class E3_2_IgnoredTest {

    @Test
    void ignoredTest() {
        System.out.println("Wont run");
    }
}
