package pl.softwareskill.course.testing.examples.junit;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class E1_1_ParametrizedTest {

    private static final BigDecimal BASE_PRICE = BigDecimal.valueOf(10);
    private static final BigDecimal PRICE_PER_KM = BigDecimal.valueOf(0.1);
    private static final long FREE_SHIPPING_DISTANCE_KM = 10;
    private static final long BASE_PRICE_SHIPPING_DISTANCE_KM = 100;

    @ParameterizedTest
    @CsvSource({
            "5, 0",
            "10, 0",
            "11, 10",
            "99, 10",
            "100, 10",
            "150, 15",
    })
    void calculatesPriceForShipping(long distanceKm, BigDecimal expectedPrice) {
        // given
        var calculator = new ShippingPriceCalculator(BASE_PRICE, PRICE_PER_KM, FREE_SHIPPING_DISTANCE_KM, BASE_PRICE_SHIPPING_DISTANCE_KM);

        // when
        var result = calculator.calculateShippingPrice(distanceKm);

        // then
        assertThat(result)
                .isEqualByComparingTo(expectedPrice);
    }
}
