package pl.softwareskill.course.testing.examples.junit;

public class StatusMapper {

    public static boolean isPhysicalDeliveryToAddress(ShippingOption shippingOption) {
        switch (shippingOption) {
            case COURIER:
            case INTERNAL_POST:
                return true;
            default:
                return false;
        }
    }
}

enum ShippingOption {
    ELECTRONIC_DELIVERY,
    SELF_PICKUP,
    COURIER,
    INTERNAL_POST,
}
