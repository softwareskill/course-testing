package pl.softwareskill.course.testing.examples.junit;

import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;

@RequiredArgsConstructor
public class ShippingPriceCalculator {

    private final BigDecimal basePrice;
    private final BigDecimal pricePerKm;
    private final long freeShippingDistanceKm;
    private final long basePriceShippingDistanceKm;

    public BigDecimal calculateShippingPrice(long distanceKm) {
        if (distanceKm <= freeShippingDistanceKm) {
            return BigDecimal.ZERO;
        }

        if (distanceKm <= basePriceShippingDistanceKm) {
            return basePrice;
        }

        var additionalDistance = distanceKm - basePriceShippingDistanceKm;
        return basePrice.add(pricePerKm.multiply(BigDecimal.valueOf(additionalDistance)));
    }
}
