package pl.softwareskill.course.testing.examples.junit;

import java.math.BigDecimal;

public class GrossPriceCalculator {

    private static final BigDecimal HUNDRED = BigDecimal.valueOf(100);

    public static BigDecimal gross(BigDecimal price, BigDecimal taxPercent) {
        var base = taxPercent.divide(HUNDRED).add(BigDecimal.ONE);
        return price.multiply(base);
    }
}
